#
# Cookbook:: imaging_demo
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'chocolatey'

packages = %w(flashplayerplugin flashplayeractivex jre8 adobereader skype vlc)

chocolatey_package packages do
    action :install
end