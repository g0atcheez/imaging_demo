packages = %w(7zip.install googlechrome notepadplusplus.install git.install putty.install filezilla sysinternals)

chocolatey_package packages do
    action :install
end